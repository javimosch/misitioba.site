<?php

require 'vendor/autoload.php';

define('PREFIX', ''); //default
//define('PREFIX', '.aldgate'); //default


Flight::route('/', function(){
    $en = Flight::request()->query['en'];
    if($en == 1){
      Flight::redirect('home');
    }else{
      Flight::redirect('inicio');
    }

});
Flight::route('/inicio(/@active)', function($active){
  $active = isset($active)?$active:'home';
  Flight::render('home'.PREFIX, array('active' => $active,'lang'=>'es'));
});
Flight::route('/home(/@active)', function($active){
  $active = isset($active)?$active:'home';
  Flight::render('home'.PREFIX, array('active' => $active,'lang'=>'eng'));
});

Flight::route('/aboutus', function(){
    Flight::render('aboutus'.PREFIX, array('active' => 'aboutus','lang'=>'eng'));
});
Flight::route('/sobrenosotros', function(){
    Flight::render('aboutus'.PREFIX, array('active' => 'aboutus','lang'=>'es'));
});
Flight::route('/quienessomos', function(){
    Flight::render('aboutus'.PREFIX, array('active' => 'aboutus','lang'=>'es'));
});



Flight::route('/servicios', function(){
    Flight::render('services'.PREFIX, array('active' => 'services','lang'=>'es'));
});
Flight::route('/services', function(){
    Flight::render('services'.PREFIX, array('active' => 'services','lang'=>'eng'));
});


Flight::route('/trabajos', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'es'));
});
Flight::route('/proyectos', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'es'));
});
Flight::route('/projects', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'eng'));
});
Flight::route('/works', function(){
    Flight::render('projects'.PREFIX, array('active' => 'projects','lang'=>'eng'));
});



Flight::route('/attorneys', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'eng'));
});
Flight::route('/nuestrosabogados', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});
Flight::route('/equipo', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});
Flight::route('/team', function(){
    Flight::render('home'.PREFIX, array('active' => 'team','lang'=>'es'));
});


Flight::route('/contacto', function(){
    Flight::render('contact'.PREFIX, array('active' => 'contact','lang'=>'es'));
});
Flight::route('/contact', function(){
    Flight::render('contact'.PREFIX, array('active' => 'contact','lang'=>'eng'));
});

//Config
//Flight::set('flight.views.path', '/path/to/views');

Flight::start();

 ?>

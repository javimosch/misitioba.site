<!DOCTYPE html>
<html lang="es" class=" desktop landscape">
	<head>
		<title>MisitioBA | Templates HTML5 Web Design Desarrollo</title>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		
		<link rel="stylesheet" href="./css/bootstrap.min.css">
		<link rel="stylesheet" href="./css/magnific-popup.css">

		<link rel="stylesheet" href="./css/animate.css">
		
		<link rel="stylesheet" href="./css/misitioba.css">
		<link rel="stylesheet" href="./css/misitioba.header.css">
		<link rel="stylesheet" href="./css/misitioba.lists.css">
		<link rel="stylesheet" href="./css/misitioba.nav.css">
		<link rel="stylesheet" href="./css/misitioba.footer.css">
		<link rel="stylesheet" href="./css/misitioba.home.css">

		<link href='https://fonts.googleapis.com/css?family=Alfa+Slab+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,100,700,300' rel='stylesheet' type='text/css'>
		
		<script type="text/javascript" src="js/vendor/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="js/vendor/jquery.magnific-popup.min.js"></script>
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.js"></script>
		<script type="text/javascript" src="js/directive.template.list.js"></script>
		<script type="text/javascript" src="js/d.nav.js"></script>
		<script type="text/javascript" src="js/directive.main.js"></script>
		<!--
		<script type="text/javascript" src="js/vendor/angular.min.js"></script>
		-->
	</head>
	<body ng-controller='main'>
		<?php include 'header.php';?>
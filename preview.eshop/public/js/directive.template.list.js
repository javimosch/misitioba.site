(function() {
    var app = angular.module('directive.template.list', []);
    app.directive('templateList', function() {
        return {
            restrict: 'A',
            controller: function($timeout) {
                console.info('template.list');
                this.items = [{
                    src: 'http://scr.templatemonster.com/55100/55158-responsive-layout.jpg',
                    big: ['http://scr.templatemonster.com/55100/55158-big.jpg', 'http://scr.templatemonster.com/55100/55158-smart2-responsive.jpg', 'http://scr.templatemonster.com/55100/55158-tablet-responsive.jpg', 'http://scr.templatemonster.com/55100/55158-smart1-responsive.jpg'],
                    name: 'Dinner King'
                }, {
                    src: 'http://scr.templatemonster.com/54800/54819-responsive-layout.jpg',
                    big: ['http://scr.templatemonster.com/54800/54819-big.jpg'],
                    name: 'Lone Star'
                }];
                this.init = function(item, $index) {
                    var data = (function() {
                        var arr = [];
                        item.big.forEach(function(big) {
                            arr.push({
                                src: big,
                                type: 'image'
                            });
                        });
                        return arr;
                    })();
                    /*
                                        $('#ID'+$index).magnificPopup({
                                            items: {
                                                src: 'http://scr.templatemonster.com/54800/54819-big.jpg'
                                            },
                                            type: 'image' // this is default type
                                        });
                    */
                    $('#ID' + $index).magnificPopup({
                        items: [{
                            src: 'http://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Peter_%26_Paul_fortress_in_SPB_03.jpg/800px-Peter_%26_Paul_fortress_in_SPB_03.jpg',
                            title: 'Peter & Paul fortress in SPB'
                        }, {
                            src: 'http://vimeo.com/123123',
                            type: 'iframe' // this overrides default type
                        }, {
                            src: $('<div class="white-popup">Dynamically created element</div>'), // Dynamically created element
                            type: 'inline'
                        }, {
                            src: '<div class="white-popup">Popup from HTML string</div>', // HTML string
                            type: 'inline'
                        }, {
                            src: '#my-popup', // CSS selector of an element on page that should be used as a popup
                            type: 'inline'
                        }],
                        gallery: {
                            enabled: true
                        },
                        type: 'image' // this is a default type
                    });
                    console.log($('#ID' + $index));
                };
            },
            controllerAs: 'list',
            templateUrl: 'views/html/template.list.html'
        }
    });
})();
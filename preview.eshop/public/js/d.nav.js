(function() {
    var app = angular.module('d.nav', []);
    app.directive('mbaNav', function() {
        return {
            restrict: 'A',
            controller: ['$window', '$timeout', '$scope', function($window, $timeout, $scope) {
                console.log('d.nav.ctrl');
                var self = this;
                var items = ['home','services','browse'];
                (function(list){
                    list.forEach(function(v,i){
                        self[v] = true;
                    });
                })(items);
                items.show = function(list){
                    
                };
                //
                self.active = '';
                self.scrolling = false;
                $scope.$watch(function() {
                    return $window.scrollY;
                }, function(scrollY) {
                    /* logic */
                    console.log(scrollY);
                    if(scrollY==0){
                        self.home=true;
                        self.services=true;
                        self.browse=true;
                        self.scrolling = false;
                    }else{
                        self.scrolling = true;
                        self.services=false;
                        self.browse=false;
                    }
                });
                self.init=function(p){
                    self.active = p.active;
                };
            }],
            controllerAs:'nav',
            link: function($scope, element, attrs, controller) {
                angular.element(window).bind("scroll", function() {
                    $scope.$apply();
                });
            }
        }
    });
})();
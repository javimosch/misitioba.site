<?php

require 'vendor/autoload.php';

Flight::route('/', function(){
    Flight::render('home', array('name' => 'Bob'));
});
Flight::route('/portada', function(){
    Flight::render('home', array('name' => 'Bob'));
});


Flight::route('/servicios', function(){
    Flight::render('services', array('name' => 'Bob'));
});

Flight::route('/portfolio', function(){
    Flight::render('portfolio', array('name' => 'Bob'));
});

//Config
//Flight::set('flight.views.path', '/path/to/views');

Flight::start();

 ?>

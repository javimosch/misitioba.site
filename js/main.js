
function redirect(url){
  window.location.href = url;
}


function setNavActive(){
  var li = null;
  $(".top-nav li").each(function(){
      var href = $(this).find('a').first().attr('href');
      var url = window.location.href;
      if(url.toString().indexOf(href)>0){
        li = $(this);
      }
  });
  $(".top-nav li").each(function(){
    $(this).removeClass('active');
  });
  li.addClass('active');
}

$(function(){
  setNavActive();

  $(".top-nav a").click(function(){
      var href = $(this).attr('href');
      console.log(href.toString().indexOf('#'));
      if(href.toString().indexOf('#')!=-1){
        $(".top-nav a").each(function(){
          $(this).parent().removeClass('active');
        });
          $(this).parent().addClass('active');
      }
  });
});

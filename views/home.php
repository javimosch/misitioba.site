<?php
  //SCOPE
 ?>
<?php include 'partials/top.php';?>

<?php include('partials/nav.php'); ?>

<div class="banner text-center">
	<div class="container">
		<div class="banner-info">
			<h1><span>Justo</span> para vos</h1>
			<p>Dise&ntilde;o | Desarrollo web y marketing digital para tu empresa</p>			<label class="page-scroll"><a class="big-btn scroll" href="#work1"><span> </span></a></label>
		</div>
	</div>
</div>

<?php include('partials/work.php'); ?>
<?php include('partials/team.php'); ?>
<?php include('partials/contact.php'); ?>
<?php include('partials/footer.php'); ?>


 <?php include 'partials/bottom.php';?>

<?php include 'partials/base_top.php';?>
<div id="content-wrapper">
		<div class="blocks-container">

			<!-- BLOCK "TYPE 3" -->
			<div class="block type-3">
				<img class="center-image" src="img/background_contact_top.jpg" alt="" />
				<div class="container">
					<div class="row">
						<div class="block-header col-xs-12">
							<div class="block-header-wrapper">
								<div class="row">

									<?php if($lang == 'es'){ ?>
										<div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">
											<h2 class="title"><span class="first">Nuestro</span>Contacto</h2>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">
											<div class="text">Nuestros días y horarios de atención personal o telefónica son de Lunes a Viernes de 10:00 am a 05:00 pm. Fuera de ese horario, puede comunicarse vía mail, mediante un mensaje vía Skype, o bien, llenando el formulario de contacto, más abajo.</div>
										</div>
						      <?php }else{ ?>
										<div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">
											<h2 class="title"><span class="first">Our</span>Contact</h2>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">
											<div class="text">Our days and times of personal or telephone operation are Monday through Friday from 10:00 am to 05:00 pm. After hours, you can contact via mail or by filling the contact form below.</div>
										</div>
						      <?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row content-block">
						<div class="col-sm-12 col-md-4 address-info">


							<?php if($lang == 'es'){ ?>
								<h3 class="title">Información sobre nuestras oficinas.</h3>
								<div class="text">
									<p>Nuestras oficinas se encuentran ubicadas en la calle Ing. E. Butty 240, piso 5
	(Torre Laminar), C.A.B.A., a pasos del cruce entre las Avenidas Córdoba y
	Leandro N. Alem.</p>
								</div>
				      <?php }else{ ?>
								<h3 class="title">Address info.</h3>
								<div class="text">
									<p>Our offices are located at Ing. E. Butty 240, 5th floor (Laminar Building), City of Buenos Aires, close to the junction between the avenues Córdoba and Leandro N. Alem.</p>
								</div>
				      <?php } ?>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 contact-wrapper">
							<div class="contact-entry">
								<div class="contact-icon"><img src="img/c-address.png" alt=""></div>
								<div class="description">

									<?php if($lang == 'es'){ ?>
									<div class="title">Dirección</div>
						      <?php }else{ ?>
									<div class="title">Our Address</div>
						      <?php } ?>
									<div class="text">Ing. E. Butty 240, piso 5, Ciudad Autónoma de Buenos Aires (C1106ABG).</div>
								</div>
							</div>
							<div class="contact-entry">
								<div class="contact-icon"><img src="img/c-email.png"  alt=""></div>
								<div class="description">
									<div class="title">E-mail</div>
									<div class="text">
                    <a href="mailto:info@oscarculariabogados.com">
                      info@oscarculariabogados.com
                    </a>
                  </div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 contact-wrapper">
							<div class="contact-entry">
								<div class="contact-icon"><img src="img/c-phone.png" alt=""></div>
								<div class="description">

									<?php if($lang == 'es'){ ?>
										<div class="title">Teléfono de contacto</div>
										<div class="text">Oficina: <a href="tel:+541144548308">+(5411) 4454-8308</a>
	                  </div>
									<?php }else{ ?>
										<div class="title">Our Phones</div>
										<div class="text">Office: <a href="tel:+541144548308">+(5411) 4454-8308</a>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="contact-entry">
								<div class="contact-icon"><img src="img/c-skype.png"  alt=""></div>
								<div class="description">
									<div class="title">Skype</div>
									<div class="text">Oscar Culari: oscar.culari</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- BLOCK "TYPE 2" -->
			<div class="block type-2">
				<div class="container">
					<div class="row">
						<div class="breadcrumbs post fadeInUp col-xs-12">

							<?php if($lang == 'es'){ ?>
								<ul>
									<li><a href="#inicio">Inicio</a></li>
									<li>Contacto</li>
								</ul>
							<?php }else{ ?>
								<ul>
									<li><a href="#home">Home</a></li>
									<li>Contact</li>
								</ul>
							<?php } ?>
						</div>
					</div>
					<div class="row">
						<div class="block-header col-xs-12">
							<div class="block-header-wrapper">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-5 post animated fadeInLeft">

										<?php if($lang == 'es'){ ?>
											<h2 class="title"><span class="first">Contacto</span>Formulario</h2>
										<?php }else{ ?>
											<h2 class="title"><span class="first">Contact</span>Form</h2>
										<?php } ?>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-7 post animated fadeInRight">
										<div class="text"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row post animated fadeInUp">
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
							<img class="img-responsive center-block" src="img/contact-from.png" alt="">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 form-block">
							<form onSubmit="return submitForm();" action="./" method="post" name="contactform" id="contact-form">
								<!-- <div class="form-text">Required fields are <span class="text-blue">*</span>. Fill out the form and we'll contact you soon</div>
                -->

								 <?php if($lang == 'es'){ ?>
									 <div class="form-text">Los campos requeridos tienen <span class="text-blue">*</span>. Complete el formulario y nos contactaremos a la brevedad.</div>
								 <?php }else{ ?>
									 <div class="form-text">Required fields are marked with <span class="text-blue">*</span>. Fill the form and we'll get back to you soon.</div>
								 <?php } ?>
								<div class="row">

									<?php if($lang == 'es'){ ?>
										<div class="col-xs-12 col-sm-6">
											<input class="form-input" name="name" type="text" required="" placeholder="Nombre *">
										</div>
										<div class="col-xs-12 col-sm-6">
											<input class="form-input" name="email" type="text" required="" placeholder="E-mail *">
										</div>
										<div class="col-xs-12 col-sm-6">
											<input class="form-input" name="phone" type="text" required="" placeholder="Telefono *">
										</div>
										<div class="col-xs-12 col-sm-6">
											<input class="form-input" name="subject" type="text" required="" placeholder="Tema *">
										</div>
										<div class="col-xs-12">
											<textarea class="form-input" name="message" placeholder="Mensaje"></textarea>
										</div>
										<div class="col-xs-12">
											<span class="button"><button class="submit" type="submit">Enviar</button></span>
											<span class="success"></span>
										</div>
 								 <?php }else{ ?>
									 <div class="col-xs-12 col-sm-6">
										 <input class="form-input" name="name" type="text" required="" placeholder="Name *">
									 </div>
									 <div class="col-xs-12 col-sm-6">
										 <input class="form-input" name="email" type="text" required="" placeholder="E-mail *">
									 </div>
									 <div class="col-xs-12 col-sm-6">
										 <input class="form-input" name="phone" type="text" required="" placeholder="Phone *">
									 </div>
									 <div class="col-xs-12 col-sm-6">
										 <input class="form-input" name="subject" type="text" required="" placeholder="Subject *">
									 </div>
									 <div class="col-xs-12">
										 <textarea class="form-input" name="message" placeholder="Message"></textarea>
									 </div>
									 <div class="col-xs-12">
										 <span class="button"><button class="submit" type="submit">Send</button></span>
										 <span class="success"></span>
									 </div>
 								 <?php } ?>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>

            <div>
                <div id="map-canvas" data-lat="58.3817" data-lng="34.6008151" data-zoom="16">

                </div>
                <div class="addresses-block">

                  <!--
										<a data-lat="58.3817" data-lng="34.6033" data-string="1. Here is some address or email or phone or something else..."></a>
                    <a data-lat="40.725604" data-lng="-73.445888" data-string="2. Here is some address or email or phone or something else..."></a>
                  -->
                </div>
            </div>

		</div>
	</div>
<?php include 'partials/base_bottom.php';?>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript">
$(function() {

	var marker = [], infowindow = [], map, image = 'img/marker.png';;

	function addMarker(location,name,contentstr){
        marker[name] = new google.maps.Marker({

            position: location,
            map: map,
			icon: image
        });
        marker[name].setMap(map);

		infowindow[name] = new google.maps.InfoWindow({
			content:contentstr
		});

		google.maps.event.addListener(marker[name], 'click', function() {
			infowindow[name].open(map,marker[name]);
		});
    }

	function initialize() {

		var lat = $('#map-canvas').attr("data-lat");
		var lng = $('#map-canvas').attr("data-lng");

		var myLatlng = new google.maps.LatLng(-34.5983988,-58.3713508);

		var setZoom = parseInt($('#map-canvas').attr("data-zoom"));

		var styles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c4c4c4"},{"visibility":"on"}]}];
		var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});

		var mapOptions = {
			zoom: setZoom,

			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},
			streetViewControl: true,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.LEFT_BOTTOM
			},

			center: myLatlng,
			mapTypeControlOptions: {
			  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			}

		};
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

		map.mapTypes.set('map_style', styledMap);
  		map.setMapTypeId('map_style');


		//$('.addresses-block a').each(function(){
			//var mark_lat = $(this).attr('data-lat');
		//	var mark_lng = $(this).attr('data-lng');
		//	var this_index = $('.addresses-block a').index(this);
		//	var mark_name = 'template_marker_'+this_index;
			var mark_locat = new google.maps.LatLng(-34.5987988, -58.3715108);
		//	var mark_str = $(this).attr('data-string');
		//	addMarker(mark_locat,mark_name,mark_str);
			addMarker(mark_locat,'office1','Oficina');
	//	});

	}

	$(window).load(function(){
		setTimeout(function(){initialize();}, 500);
	});

});
</script>

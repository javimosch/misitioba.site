<?php include 'partials/base_top.php';?>
<div id="content-wrapper">
		<div class="blocks-container">

			<!-- BLOCK "TYPE 3" -->
			<div class="block type-3">
				<img class="center-image" src="img/background_aboutus.jpg" alt="" />
				<div class="container">
					<div class="row">
						<div class="block-header col-xs-12">
							<div class="block-header-wrapper">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">
										<?php if($lang == 'es'){ ?>
							      	<h2 class="title"><span class="first">Quienes </span>Somos</h2>
							      <?php }else{ ?>
							        <h2 class="title"><span class="first"> </span>About Us</h2>
							      <?php } ?>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">
										<div class="text"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<!-- BLOCK "TYPE 7" -->
			<div class="block type-7">
				<div class="container">
					<div class="row">

						<div class="col-xs-12">
							<div class="media">
							  <div class="media-left">
								  <img class="corner-rounding media-object" src="img/background_aboutus2.jpg" alt="">
							  </div>
							  <div class="media-body text">
								<!-- <h2 class="title">Quienes Somos</h2> -->


								<?php if($lang == 'es'){ ?>
									<p>
										Somos un grupo de profesionales destacados por nuestra trayectoria tanto en el área

		del litigio como en el área académica. Nuestro objetivo principal es defender el interés

		de nuestro cliente y buscar los mayores beneficios dentro de cada contexto dado. Por

		ello, nunca descartamos el análisis socio-económico ni macro-ecónomico. Nuestros

		conocimientos en finanzas le dan un valor agregado a ese análisis posicionando a

		nuestros clientes en una situación ventajosa.


									</p>

									<p>Tenemos como valor fundamental, el trabajo en equipo. No concebimos la idea de tener

									miembros en el staff con conocimientos mesiánicos. Por el contrario, somos de la idea

									que la realidad de nuestros días, el contexto global y local, nos obliga a un análisis

									multidiciplinario y a un trabajo en equipo para superar cada desafío que se nos plantea.

									Creémos firmemente que en el campo del trabajo en equipo, el todo es más que la

									suma de las partes.</p>
								<?php }else{ ?>
									<p>
										We are a group of outstanding professionals by our track record both in the area of litigation and academics. Our main goal is to defend the interests of our client and seek the greatest benefits within each given context. Therefore, we never discount the socio-economic and macro-economic analysis. Our expertise in finance give added value to the analysis positioning our clients in an advantageous situation.
									</p>
									<p>We as a fundamental value, teamwork. We do not conceive the idea of having staff members with messianic knowledge. On the contrary, we are of the idea that the reality of today, global and local context requires us to a multidisciplinary analysis and teamwork to overcome every challenge we face. We firmly believe that in the field of teamwork, the whole is more than the sum of its parts.</p>
								<?php } ?>

							  </div>
							</div>
						</div>
					</div>
					<div class="row hidden">
						<div class="icon-entry col-xs-12 col-sm-6">
							<img class="icon" src="img/icon-1.png" alt="">
							<div class="description">
								<div class="title">our philosophy</div>
								<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh ex, cursus eget porta eget, consequat quis nisi. Nullam tincidunt gravida augue nec tempor. Nunc eu velit dictum, dignissim eros quis, consectetur ligula.</div>
							</div>
						</div>
						<div class="icon-entry col-xs-12 col-sm-6">
							<img class="icon" src="img/icon-2.png" alt="">
							<div class="description">
								<div class="title">our principles</div>
								<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh ex, cursus eget porta eget, consequat quis nisi. Nullam tincidunt gravida augue nec tempor. Nunc eu velit dictum, dignissim eros quis, consectetur ligula.</div>
							</div>

						</div>
						<div class="icon-entry col-xs-12 col-sm-6">
							<img class="icon" src="img/icon-4.png" alt="">
							<div class="description">
								<div class="title">our vision</div>
								<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh ex, cursus eget porta eget, consequat quis nisi. Nullam tincidunt gravida augue nec tempor. Nunc eu velit dictum, dignissim eros quis, consectetur ligula. Cras ac cursus dui, in convallis massa. Mauris pellentesque velit urna, ac vestibulum.</div>
							</div>
						</div>
						<div class="icon-entry col-xs-12 col-sm-6">
							<img class="icon" src="img/icon-3.png" alt="">
							<div class="description">
								<div class="title">our future</div>
								<div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh ex, cursus eget porta eget, consequat quis nisi. Nullam tincidunt gravida augue nec tempor. Nunc eu velit dictum, dignissim eros quis, consectetur ligula. Cras ac cursus dui, in convallis massa. Mauris pellentesque velit urna, ac vestibulum.</div>
							</div>
						</div>
					</div>

				</div>
			</div>


			<?php include 'partials/beneficios.php';?>
			<?php include 'partials/abogados.php';?>
			<?php include 'partials/resultados.php';?>




		</div>
	</div>

<?php include 'partials/base_bottom.php';?>

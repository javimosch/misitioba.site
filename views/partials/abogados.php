<div class="" id="nuestrosabogados"></div>
<div class="" id="attorneys"></div>

<!-- BLOCK "TYPE 4" -->
<div class="block type-4">
  <div class="container">
    <div class="row">
      <div class="block-header col-xs-12">
        <div class="block-header-wrapper">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">

              <?php if($lang == 'es'){ ?>
                <h2 class="title"><span class="first">Nuestros</span>Abogados</h2>
              <?php }else{ ?>
                <h2 class="title"><span class="first">Our</span>Attorneys</h2>
              <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">
              <?php if($lang == 'es'){ ?>
                <div class="text"></div>
              <?php }else{ ?>
                <div class="text">Our team's main argument, his career both in the field of practice and in the academic area. From there, it is where our best advice to clients have a complete idea about your specific situation as a categorical diagnosis of each subject.</div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row team">



	<div class="icon-entry col-sm-6 col-md-3 post fadeInUp">
        <img class="corner-rounding" src="img/martin_pepe.jpg" alt="">
        <div class="content">
          <div class="information">
            <h3 class="name">Marcelo A. Pepe</h3>
          <?php if($lang == 'es'){ ?>
              <div class="job">Abogado – Profesor – Consultor – Traductor Público</div>
            </div>
            <div class="text">Abogado – Profesor de Derecho Civil, Comercial y Notarial (Universidad de Buenos Aires,
  		Universidad Nacional de La Plata) - Especialista en Derecho Documental. Traductor Público.</div>
          <?php }else{ ?>
              <div class="job">Lawyer – Professor – Consultant</div>
            </div>
            <div class="text">Attorney – Professor of Civil law, Commercial law and Notarial law (University of Buenos Aires, National University of La Plata and University of Cambridge) - Comparative Law expert.</div>
          <?php } ?>
          <div class="social-block">
            <a target="_blank" href="https://ar.linkedin.com/pub/marcelo-pepe/58/49/882"><img src="img/info-in.png" alt=""></a>
            <a target="_blank" href="mailto:info@oscarculariabogados.com"><img src="img/info-email.png" alt=""></a>
          </div>
                      </div>
      </div>

      <div class="icon-entry col-sm-6 col-md-3 post fadeInUp">
        <img class="corner-rounding" src="img/oscar_culari.jpg" alt="">
        <div class="content">
            <div class="information">
              <h3 class="name">Oscar Culari</h3>
          <?php if($lang == 'es'){ ?>
              <div class="job">Magister – Abogado – Asesor Financiero Certificado</div>
            </div>
            <div class="text">Magister en Finanzas con especialización en Mercados de Capitales y Abogado con especialización en Derecho Privado. Asesor Financiero Certificado.</div>
          <?php }else{ ?>
            <div class="job">Master – Lawyer – Financial Advisor</div>
            </div>
            <div class="text">Master in Finance, focusing on Capital Markets. Lawyer with specialization in Private Law. Certified Financial Advisor.</div>
          <?php } ?>
          <div class="social-block">
            <a target="_blank" href="https://ar.linkedin.com/pub/oscar-alfredo-culari/46/a4b/b71"><img src="img/info-in.png" alt=""></a>
            <a target="_blank" href="mailto:info@oscarculariabogados.com"><img src="img/info-email.png" alt=""></a>
          </div>
                      </div>
      </div>
      <div class="clearfix visible-sm"></div>
      <div class="icon-entry col-sm-6 col-md-3 post fadeInUp">
        <img class="corner-rounding" src="img/laura_stieben.jpg" alt="">
        <div class="content">
          <div class="information">
            <h3 class="name">Laura Stieben</h3>

          <?php if($lang == 'es'){ ?>
            <div class="job">Abogada</div>
          </div>
          <div class="text">Abogada egresada de la Universidad de Buenos Aires, con especialización en Derecho Empresarial.</div>
          <?php }else{ ?>
            <div class="job">Lawyer</div>
          </div>
          <div class="text">Attorney graduated from the University of Buenos Aires, specializing in Business Law.</div>
          <?php } ?>
          <div class="social-block">
            <a target="_blank" href="https://ar.linkedin.com/pub/laura-stieben/65/194/12b"><img src="img/info-in.png" alt=""></a>
            <a target="_blank" href="mailto:info@oscarculariabogados.com"><img src="img/info-email.png" alt=""></a>
          </div>
                      </div>
      </div>

      <div class="icon-entry col-sm-6 col-md-3 post fadeInLeft">
        <img class="corner-rounding" src="img/martin_caraballo.jpg" alt="">
        <div class="content">
          <div class="information">
            <h3 class="name">Martín Caraballo</h3>

          <?php if($lang == 'es'){ ?>
            <div class="job">Abogado</div>
          </div>
          <div class="text">Abogado egresado de la Universidad de Buenos Aires, con especialización en Derecho Laboral.</div>
          <?php }else{ ?>
            <div class="job">Lawyer</div>
          </div>
          <div class="text">Attorney graduated from the University of Buenos Aires, specializing in Business Law.</div>
          <?php } ?>
          <div class="social-block">
            <a target="_blank" href="#"><img src="img/info-in.png" alt=""></a>
            <a target="_blank" href="mailto:info@oscarculariabogados.com"><img src="img/info-email.png" alt=""></a>
          </div>
                      </div>
      </div>


    </div>
    <div class="row text-center hidden">
      <span class="button"><a href="attorneys.html">all Attorneys</a></span>
    </div>

  </div>
</div>

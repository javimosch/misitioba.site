

<div id="camera" class="camera-wrap">
    <div data-src="images/index-slide01.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>
                                Diseña tu próximo paso...
                                <span></span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide02.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>
                            Integrada por profesionales 
                                <span>con amplio conocimiento y experiencia en negocios financieros.</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide03.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>Nuestros objetivos son
                                <span>minimización de riesgos y maximización de beneficios para nuestros clientes</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <div id="owl">
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img01.png" alt="Image 1"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Bienvenidos a Aldgate Advisors!
                                </h3>
                                <p class="quote_txt">
                                    “Diseños de carteras y estrategias a medida de cada inversor.”
                                </p>
                                <p class="quote_author">
                                   Mg. Diego Demarco.
                                   <br>
                                   Managing Partner. Master en Finanzas (UCEMA), Economista (UBA) y Master en Administración de Empresas (IAS & LBS).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img02.png" alt="Image 2"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Bienvenidos a Aldgate Advisors!
                                </h3>
                                <p class="quote_txt">
                                    “Nos ocupamos de mantenerte informado sobre lo que pasa en el mercado local e internacional y cómo ello, repercute en cada portafolio.”
                                </p>
                                <p class="quote_author">
                                   Mg. Daiana Galun.
                                   <br>
                                   Managing Partner. Master en Finanzas (UCEMA), Economista (UBA). Cursó estudios de negocios en EADA (Barcelona).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img03.png" alt="Image 3"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Bienvenidos a Aldgate Advisors!
                                </h3>

                                <p class="quote_txt">
                                    ““Nos apasiona pensar en su próximo paso...”
                                </p>

                                <p class="quote_author">
                                   Mg. Oscar Culari.
                                   <br>
                                   Managing Partner. Master en Finanzas (UCEMA), Abogado con especialización en Derecho Privado (UBA) y Asesor Financiero Certificado (IAEF).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/home_bandi.jpg" alt="Image 3"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Bienvenidos a Aldgate Advisors!
                                </h3>

                                <p class="quote_txt">
                                    “Compromiso con su patrimonio y su futuro.”
                                </p>

                                <p class="quote_author">
                                   Mg. Alejandro Bandi.
                                   <br>
                                   Managing Partner. Es contador (UBA). Master en Finanzas (UCEMA).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</header>

<!--========================================================
                          HEADER
=========================================================-->
<header id="header">
<div class="cnt">
    <div class="substrate"></div>
    <div id="stuck_container">
        <div class="container">
            <div class="row">
                <div class="grid_12">

                    <div class="brand flt__l flt__n-sm">
                        <div class="brand_wr">
                            <h1>
                                <a href="./">
                                    Wealth
                                    <span>investment</span>
                                </a>
                            </h1>
                        </div>
                    </div>

                    <div class="js-search flt__r flt__n-sm">
                        <form id="search" class="sform" action="search/search.php" method="GET"
                              accept-charset="utf-8">
                            <label data-type="input" for="in">
                                <input id="in" type="text" name="s" value="Search..."
                                       onblur="if(this.value == '') { this.value='Search...'}"
                                       onfocus="if (this.value == 'Search...') {this.value=''}"/>
                            </label>
                            <a data-type="submit" onclick="document.getElementById('search').submit()"></a>
                        </form>
                    </div>

                    <nav class="nav flt__l flt__n-lg">
                        <ul class="sf-menu">
                            <li class="current">
                                <a href="./">Home</a>
                            </li>
                            <li>
                                <a href="index-1.html">Profile</a>
                                <ul>
                                    <li>
                                        <a href="#">Lorem</a>
                                    </li>
                                    <li>
                                        <a href="#">Ipsum</a>
                                        <ul>
                                            <li>
                                                <a href="#">Massa</a>
                                            </li>
                                            <li>
                                                <a href="#">Laoreetum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Dolore</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="index-2.html">Services</a>
                            </li>
                            <li>
                                <a href="index-3.html">Projects</a>
                            </li>
                            <li>
                                <a href="index-4.html">Blog</a>
                            </li>
                            <li>
                                <a href="index-5.html">Contacts</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="camera" class="camera-wrap">
    <div data-src="images/index-slide01.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>Advice on how
                                <span>to make wise investments</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide02.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>
                Exceptional investments, 
                                <span>extraordinary world</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide03.jpg">
        <div class="fadeIn camera_caption">
            <div class="container">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>Global leader 
                                <span>in investment services</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <div id="owl">
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/index_img01.jpg" alt="Image 1"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to our website!
                                </h3>

                                <p class="quote_txt">
                                    “ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud
                                    exercitation ullamco laboris nisi ut aliquip. ”
                                </p>

                                <p class="quote_author">
                                    mr. John Grew, (manager)
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/index_img02.jpg" alt="Image 2"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to our website!
                                </h3>

                                <p class="quote_txt">
                                    “ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud
                                    exercitation ullamco laboris nisi ut aliquip. ”
                                </p>

                                <p class="quote_author">
                                    mr. John Grew, (manager)
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/index_img03.jpg" alt="Image 3"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to our website!
                                </h3>

                                <p class="quote_txt">
                                    “ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                    nostrud
                                    exercitation ullamco laboris nisi ut aliquip. ”
                                </p>

                                <p class="quote_author">
                                    mr. John Grew, (manager)
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</header>

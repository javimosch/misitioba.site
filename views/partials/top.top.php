<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MISITIOBA | Desarrollo Web, Diseño, Programación</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->


        <link rel="stylesheet" type="text/css" href="http://p.w3layouts.com/demos/easier/web/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Playball">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <link rel="stylesheet" type="text/css" href="http://p.w3layouts.com/demos/easier/web/css/jquery.hoverGrid.css">
        <link rel="stylesheet" href="css/app.css" type="text/css" media="screen" />



<div id="camera" class="camera-wrap">
    <div data-src="images/index-slide01.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container ">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>Designing your next step ...
                                <span></span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide02.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container ">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>
                            We involve profesionals
                                <span>with knowledge and experience</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-src="images/index-slide03.aldgate.jpg">
        <div class="fadeIn camera_caption">
            <div class="container ">
                <div class="row">
                    <div class="grid_12">
                        <div class="camera_caption_cnt">
                            <h2>Our objectives
                                <span>
                                minimizing risks and maximizing benefits 
                                </span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg1">
    <div class="container">
        <div class="row">
            <div class="grid_12">
                <div id="owl">
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img01.png" alt="Image 1"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to Aldgate Advisors!
                                </h3>
                                <p class="quote_txt">
                                    “We design portfolios and strategies to suit each investor.”
                                </p>
                                <p class="quote_author">
                                   Mg. Diego Demarco.
                                   <br>
                                   Managing Partner. Master in Finance (UCEMA), Economist (UBA) and Master in Business Administration (IAS & LBS).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img02.png" alt="Image 2"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to Aldgate Advisors!
                                </h3>
                                <p class="quote_txt">
                                    “We take care to stay informed about what is happening in the local and international markets and how this has an impact on each portfolio.”
                                </p>
                                <p class="quote_author">
                                   Mg. Daiana Galun.
                                   <br>
                                   Managing Partner. Master in Finance (UCEMA), Economist (UBA). He studied business at EADA (Barcelona).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/index_img03.png" alt="Image 3"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to Aldgate Advisors!
                                </h3>

                                <p class="quote_txt">
                                    “We are passionate thinking about your next step...”
                                </p>

                                <p class="quote_author">
                                   Mg. Oscar Culari.
                                   <br>
                                   Managing Partner. Master in Finance (UCEMA), Lawyer specializing in Private Law (UBA) and Certified Financial Planner (IAEF).
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="quote">
                            <div class="quote_aside">
                                <img src="images/aldgate/home_bandi.jpg" alt="Image 3"/>
                            </div>
                            <div class="quote_cnt">
                                <h3 class="quote_ttl">
                                    Welcome to Aldgate Advisors!
                                </h3>

                                <p class="quote_txt">
                                    “Commitment to your heritage and your future.”
                                </p>

                                <p class="quote_author">
                                   Mg. Alejandro Bandi.
                                   <br>
                                   Managing Partner. Accountant (UBA). Master in Finance (UCEMA).
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</header>

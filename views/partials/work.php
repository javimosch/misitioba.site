﻿<div id="work" class="work team-head">
	<div id="work1" class="container">
		<div class="head-one text-center">
			<h2>trabajos</h2>
			<span> </span>
			<p>Algunas paginas web realizadas en nuestro tiempo libre</p>
		</div>



		<div class="works">
			<div id="whatever">
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_quadramma.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Quadramma</h2>
							<p>programacion</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_clarity.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Clarity</h2>
							<p>diseño + programacion</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_gailum.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Gailuminacion</h2>
							<p>programacion</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_misspies.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Miss Pies</h2>
							<p>programacion</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_clearfilm.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Clear Films</h2>
							<p>programacion</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 work-grid">
					<div class="item">
						<img  src="img/thumb_misitioba.jpg" title="name" />
						<div class="caption" style="display: none;">
							<h2>Misitioba</h2>
							<p>programacion</p>
						</div>
					</div>
				</div>



				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>

<!-- BLOCK "TYPE 3" -->
<div class="block type-3">
  <img class="center-image" src="img/background_benefits.jpg" alt="" />
  <div class="container">
    <div class="row">
      <div class="block-header col-xs-12">
        <div class="block-header-wrapper">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">

              <?php if($lang == 'es'){ ?>
                <h2 class="title"><span class="first">Nuestros</span>beneficios</h2>
              <?php }else{ ?>
                <h2 class="title"><span class="first">Our</span>benefits</h2>
              <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">

              <?php if($lang == 'es'){ ?>
                <div class="text">Atento a la singularidad de cada integrante de nuestro equipo como también, al “todo” que logramos cuando nos combinamos entre nosotros, nos cuesta determinar todos los beneficios que podemos brindar a nuestros clientes. Pero, a modo de síntesis podemos mencionar:</div>
              <?php }else{ ?>
                <div class="text">Attentive to the uniqueness of each member of our team as well, the "whole" we achieved when we combine between us, it costs us to determine all the benefits we can provide our customers. But, by way of summary we can mention:</div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
              <div class="row content-block post fadeInUp">
      <div class="col-xs-12 col-sm-6">
        <h3 class="title"></h3>
        <div class="text">
          <p></p>
          <ul>

            <?php if($lang == 'es'){ ?>
              <li>Trayectoria. √</li>
              <li>Conocimiento aplicado. √</li>
              <li>Cercanía. √</li>
              <li>Confianza. √</li>
              <li>Confidencialidad. √</li>
              <li>Talento. √</li>
            <?php }else{ ?>
              <li>Career. √</li>
              <li>Knowledge. √</li>
              <li>Nearness. √</li>
              <li>Confidence. √</li>
              <li>Confidentiality. √</li>
              <li>Talent. √</li>
            <?php } ?>

          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <h3 class="title"></h3>
        <div class="text">
          <p></p>
          <ul>

            <?php if($lang == 'es'){ ?>
              <li>Liderazgo. √</li>
              <li>Conocimiento. √</li>
              <li>Inteligencia emocional. √</li>
              <li>Seguridad. √</li>
              <li>Compromiso. √</li>
              <li>Pasión por lo que hacemos. √</li>
            <?php }else{ ?>
              <li>Leadership. √</li>
              <li>Applied knowledge. √</li>
              <li>Emotional intelligence. √</li>
              <li>Sureness. √</li>
              <li>Commitment. √</li>
              <li>Passion about what we do. √</li>
            <?php } ?>
          </ul>
        </div>
      </div>
              </div>

  </div>
</div>


<div class="l-content">
    <div class="pricing-tables pure-g">
        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-free">
                <div class="pricing-table-header">
                    <h2>Template</h2>

                    <span class="pricing-table-price">

                    </span>
                </div>

                <ul class="pricing-table-list">
                    <li>Template HTML5</li>
                    <li>Adaptacion de contenido</li>
                    <li>SEO basico</li>
                    <li>Mobile / Responsive</li>
                    <li>Soporte tecnico por email</li>
                    <li>Sin plan de hosting</li>
                </ul>

            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-biz pricing-table-selected">
                <div class="pricing-table-header">
                    <h2>Personalizado</h2>

                    <span class="pricing-table-price">

                    </span>
                </div>

                <ul class="pricing-table-list">
                    <li>Template HTML5</li>
                    <li>Rediseño y Adaptacion</li>
                    <li>SEO avanzado</li>
                    <li>Mobile / responsive</li>
                    <li>Soporte tecnico por email/telefono</li>
                    <li>1 año de hosting + dominio .com</li>
                </ul>

            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-3">
            <div class="pricing-table pricing-table-enterprise">
                <div class="pricing-table-header">
                    <h2>Pyme</h2>

                    <span class="pricing-table-price">
                        
                    </span>
                </div>

                <ul class="pricing-table-list">
                    <li>Diseño Web y Programacion</li>
                    <li>Administrador de Contenidos</li>
                    <li>SEO avanzado</li>
                    <li>Mobile / Responsive</li>
                    <li>Soporte tecnico ilimitado</li>
                    <li>2 años de hosting + dominio .com</li>
                </ul>
            </div>
        </div>
    </div> <!-- end pricing-tables -->

    <div class="information pure-g">
        <div class="pure-u-1 pure-u-md-1-2">
            <div class="l-box">
                <h3 class="information-head">Mayor control</h3>
                <p>
                    A diferencia de muchas plataformas web y agencias digitales, con nosotros
                    vos tenes el control sobre el codigo fuente. Donde cuando y como puede ser una decision tuya.
                </p>
            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-2">
            <div class="l-box">
                <h3 class="information-head">Un mundo de templates</h3>
                <p>
                    Cientos y cientos de templates HTML5 Responsive a tu disposicion.
                    Implementamos cualquier template que encuentres en internet.
                </p>
            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-2">
            <div class="l-box">
                <h3 class="information-head">24/7 soporte tecnico / comercial</h3>
                <p>
                    Respondemos cualquier pregunta tecnica por email en menos de 24hs y cualquier dia de la semana.
                    Necesitas actualizar contenido o realizar algun ajuste?. Nos encargamos de eso a la brevedad
                    y re-cotizamos si hace falta.
                </p>
            </div>
        </div>

        <div class="pure-u-1 pure-u-md-1-2">
            <div class="l-box">
                <h3 class="information-head">Presupuestos a medida</h3>
                <p>
                    Escuchamos tus necesidades y te planteamos soluciones a la medida de tu negocio.
                </p>
            </div>
        </div>
    </div> <!-- end information -->
</div> <!-- end l-content -->

<!--
<div class="footer l-box">
    <p>
        <a href="#">Try now</a> for 14 days. No credit card required. Header image courtesy of <a href='http://unsplash.com/'>Unsplash</a>.
    </p>
</div>
-->

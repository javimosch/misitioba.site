<div class="" id="servicios"></div>
<div class="" id="services"></div>
<!-- BLOCK "TYPE 2" -->
<div class="block type-2">
  <div class="container">
    <div class="row">
      <div class="block-header col-xs-12">
        <div class="block-header-wrapper">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-5 post animated fadeInLeft">


							<?php if($lang == 'es'){ ?>
				            <h2 class="title"><span class="first">Nuestros</span>Servicios</h2>
				      <?php }else{ ?>
				            <h2 class="title"><span class="first">Our</span>Services</h2>
				      <?php } ?>


            </div>
            <div class="col-xs-12 col-sm-6 col-md-7 post animated fadeInRight">

              <?php if($lang == 'es'){ ?>
				            <div class="text">Brindamos distintos servicios legales. Asesoramos para prevenir. Litigamos con compromiso y profesionalismo.</div>
				      <?php }else{ ?>
				            <div class="text">We provide various legal services. We advise to prevent. We litigate with commitment and professionalism.</div>
				      <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row post animated fadeInUp">
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
        <img class="img-responsive center-block" src="img/law-block.png" alt="">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 icon-blocks">
        <div class="row">
          <div class="col-xs-12 col-sm-6 services">
            <img class="icon-service" src="img/criminal-law.png" alt="">
            <div class="description">
              <?php if($lang == 'es'){ ?>
                  <div class="title">Derecho Laboral</div>
                  <div class="text">Habiendo litigado desde ambos lados (defendiendo a trabajadores como a empresarios), conocemos acabadamente los intereses y estrategias legales de cada uno.</div>
				      <?php }else{ ?>
                <div class="title">Labor law</div>
                <div class="text">Having litigated from both sides (defending workers and employers) know in detail the
legal interests and strategies of each side.</div>
				      <?php } ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 services">
            <img class="icon-service" src="img/icon-money.png" alt="">
            <div class="description">

              <?php if($lang == 'es'){ ?>
                <div class="title">Derecho Financiero</div>
                <div class="text">Asesoramos y gestionamos trámites ante la Comisión Nacional de Valores, como también, otros temas relacionados al mercado de valores.</div>
				      <?php }else{ ?>
                <div class="title">Financial Law</div>
                <div class="text">We advise and manage proceedings before the National Securities Commission, as
well as other issues related to the stock market.</div>
				      <?php } ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6 services">
            <img class="icon-service" src="img/family-law.png" alt="">
            <div class="description">

              <?php if($lang == 'es'){ ?>
                <div class="title">Derecho Empresarial</div>
                <div class="text">Analizamos y confeccionamos contratos en materia de negocios, trabajamos todo lo relativo al derecho societario como, así tambien, en lo que atañe a marcas y patentes.</div>
				      <?php }else{ ?>
                  <div class="title">Business Law</div>
                  <div class="text">We analyze and tailor contracts for businesses, worked all matters relating to
corporate law as well too, with regard to trademarks and patents.</div>
				      <?php } ?>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 services">
            <img class="icon-service" src="img/personal-injury.png" alt="">
            <div class="description">

              <?php if($lang == 'es'){ ?>
                  <div class="title">Derecho de Daños</div>
                  <div class="text">Tenemos basta experiencia en el derecho de daños, desde incumplimientos contractuales hasta daños extracontractuales. Desde luego, también trabajamos sobre la órbita del derecho relacionado a los seguros.</div>
				      <?php }else{ ?>
                  <div class="title">Personal Injury</div>
                  <div class="text">We have vast experience in tort law, from contractual breaches to tort. Of course,
we also work on the orbit of the right related to insurance.</div>
				      <?php } ?>
            </div>
          </div>
        </div>
        <div class="row text-center hidden">
          <span class="button"><a href="about-us.html">all practice areas</a></span>
        </div>
      </div>
    </div>

  </div>
</div>

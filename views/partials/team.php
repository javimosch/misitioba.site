﻿
<div id="team" class="team">
	<div class="container">
		<div class="head-one text-center team-head">
			<h2>equipo</h2>
			<span> </span>
			<p>Somos un equipo joven integrado por chicos de diferentes paises que esta dispuesto a escuchar tus necesidades y encontrar soluciones creativas.</p>
		</div>

		<div class="team-members">

			<!--

		-->

			<div class="col-md-3 col-lg-3">
				<div class="team-member text-center">
					<a href="https://about.me/arancibiajav"><img class="t-pic" src="img/logo_javier_arancibia.jpg" title="name" /></a>
					<h3>Javier L. Arancibia</h3>
					<span>fullstack developer</span>
					<ul class="t-social2 unstyled-list list-inline">
						<li><a class="twitter" href="#"><span> </span></a></li>
						<li><a class="dribbble" href="#"><span> </span></a></li>
						<li><a class="in" href="#"><span> </span></a></li>
						<div class="clearfix"> </div>
					</ul>
				</div>
			</div>

			<div class="col-md-3 col-lg-3">
				<div class="team-member text-center">
					<img class="t-pic" src="img/logo_violeta.jpg" title="name" />
					<h3>Violeta Campos</h3>
					<span>photographer</span>
					<ul class="t-social2 unstyled-list list-inline">
						<li><a class="twitter" href="#"><span> </span></a></li>
						<li><a class="dribbble" href="#"><span> </span></a></li>
						<li><a class="in" href="#"><span> </span></a></li>
						<div class="clearfix"> </div>
					</ul>
				</div>
			</div>

			<div class="col-md-3 col-lg-3">
				<div class="team-member text-center">
					<img class="t-pic" src="img/logo_richard_gentilella.jpg" title="name" />
					<h3>Richard Gentilella</h3>
					<span>industrial designer</span>
					<ul class="t-social2 unstyled-list list-inline">
						<li><a class="twitter" href="#"><span> </span></a></li>
						<li><a class="dribbble" href="#"><span> </span></a></li>
						<li><a class="in" href="#"><span> </span></a></li>
						<div class="clearfix"> </div>
					</ul>
				</div>
			</div>

			<div class="col-md-3 col-lg-3">
				<div class="team-member text-center">
					<img class="t-pic" src="img/logo_maelle.jpg" title="name" />
					<h3>Maelle Aubert</h3>
					<span>marketer</span>
					<ul class="t-social2 unstyled-list list-inline">
						<li><a class="twitter" href="#"><span> </span></a></li>
						<li><a class="dribbble" href="#"><span> </span></a></li>
						<li><a class="in" href="#"><span> </span></a></li>
						<div class="clearfix"> </div>
					</ul>
				</div>
			</div>

			<div class="clearfix"> </div>
		</div>

	</div>
</div>

<div id="contact" class="contact team-head">
	<div class="container">
		<div class="head-one text-center">
			<h2>contacto</h2>
			<span> </span>
		</div>
		<div class="contact-grids">
			<div class="col-md-5 contact-map">
				<p>Donde nos ubicamos?</p>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26270.188021685233!2d-58.42262644999997!3d-34.609888500000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcca5e41da17ff%3A0xebce9fad2698f23f!2sAlmagro%2C+Autonomous+City+of+Buenos+Aires!5e0!3m2!1sen!2sar!4v1434333541901" width="400" height="300" frameborder="0" style="border:0"></iframe>
			</div>
			<div class="col-md-7 contact-form">
				<p>Queres contactarnos ahora? Basta con escribir a...</p>

				<h1>contacto@misitioba.com</h1>
<!--
				<form>
					<textarea>Dejanos un mensaje...</textarea>
					<input type="text" value="Your@mail.com">
					<input type="submit" value="Send" />
				</form>
			-->
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>

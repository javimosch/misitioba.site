
<!--========================================================
                          HEADER
=========================================================-->
<header id="header">
<div class="cnt">
    <div class="substrate"></div>
    <div id="stuck_container">
        <div class="container">
            <div class="row">
                <div class="grid_12">

                    <div class="brand flt__l flt__n-sm">
                        <div class="brand_wr">
                            <h1>
                                <a href="./">
                                <!--
                                    Wealth
                                    <span>investment</span>
                                    -->
                                </a>
                            </h1>
                        </div>
                    </div>

                    <div class="js-search flt__r flt__n-sm hidden">
                        <form id="search" class="sform" action="#" method="GET"
                              accept-charset="utf-8">
                            <label data-type="input" for="in">
                                <input id="in" type="text" name="s" value="" placeholder="Buscar..."

                                       />
                            </label>
                            <a data-type="button" ></a>
                        </form>
                    </div>

                    <nav class="nav flt__r flt__n-lg">
                        <ul class="sf-menu">
                            <li class="<?php echo ($active=='home'?'current':'')?> ">
                                <a href="./">Inicio</a>
                            </li>
                            <li class="">
                                <a href="./servicios">Servicios</a>
                            </li>
                            <li class="">
                                <a href="./quienessomos">Quienes somos</a>
                            </li>
                            <li class="<?php echo ($active=='contact'?'current':'')?> ">
                                <a href="./contacto">Contacto</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>
</div>

<?php  
if($active=='home'){
?> 

<?php include 'header.aldgate.home.'.$lang.'.php';?>

<?php  
}else{ //else
?> 

<?php include 'header.aldgate.other.php';?>

<?php  
} //end
?> 

<!-- BLOCK "TYPE 3" -->
<div class="block type-3">
  <img class="center-image" src="img/background_ourwinincourt.jpg" alt="" />
  <div class="container">
    <div class="row">
      <div class="block-header col-xs-12">
        <div class="block-header-wrapper">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-5 post fadeInLeft">

              <?php if($lang == 'es'){ ?>
                <h2 class="title"><span class="first">Nuestros Resultados</span>ante los Tribunales</h2>
              <?php }else{ ?>
                <h2 class="title"><span class="first">Our Win</span>In Court</h2>
              <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-7 post fadeInRight">
              <!--<div class="text"></div> -->
               <!-- Nuestros Resultados en el ejercicio de la profesión ante los Tribunales.-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row content-block in-court post fadeInUp">
      <div class="col-xs-12 col-sm-12 col-md-6">
        <h3 class="title"></h3>

        <?php if($lang == 'es'){ ?>
          <p class="text">
            Nuestro trabajo en el ejercicio de la profesión, nos permite mostrar los resultados de
    los objetivos alcanzados ante los tribunales, los cuales se pueden constatar a través de
    registros públicos ofrecidos por la Corte Suprema de Justicia de la Nación y de la
    Suprema Corte de Justicia de la Provincia de Buenos Aires.
          </p>
        <?php }else{ ?>
          <p class="text">
          Our work in the practice of the profession, allows us to display the results of the objectives achieved in the courts, which can be stated through public records provided by the Supreme Court of Justice of the Nation (Corte Suprema de Justicia de la Nación) and the Supreme Court the Province of Buenos Aires (Suprema Corte de Justicia de la Provincia de Buenos Aires).
          </p>
        <?php } ?>
        <div class="row statistic">
          <div class="stat-block">
            <div class="percent"><span class="counter">99</span>%</div>

            <?php if($lang == 'es'){ ?>
              <div class="text">Pleitos laborales.<br>(Por parte del trabajador damnificado)</div>
            <?php }else{ ?>
              <div class="text">Laboral Law. – Injurys in workplace.<br>(By the injured worker).</div>
            <?php } ?>
          </div>
          <div class="stat-block">
            <div class="percent"><span class="counter">96</span>%</div>

            <?php if($lang == 'es'){ ?>
              <div class="text">Pleitos Civiles.<br>(Por parte del damnificado)</div>
            <?php }else{ ?>
              <div class="text">Personal Injury.<br>(By the injured party)</div>
            <?php } ?>
          </div>
          <div class="stat-block">
            <div class="percent"><span class="counter">99</span>%</div>

            <?php if($lang == 'es'){ ?>
              <div class="text">Accidentes de Riesgo de Trabajo.<br>(Por parte del trabajador damnificado)</div>
            <?php }else{ ?>
              <div class="text">Laboral Law. – Unjustified dismissal.<br>(By the injured worker).</div>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="hidden-xs col-xs-12 col-sm-12 col-md-6">
        <div class="chart-wrapper">
          <div class="chart-entry">
            <div class="chart">
              <div class="overflow-1">
                <div class="round-1"></div>
                <div class="color-block"></div>
                <div class="number">04</div>
              </div>
              <div class="overflow-2">
                <div class="round-2 part-1"></div>
                <div class="color-block"></div>
                <div class="number">03</div>
              </div>
              <div class="overflow-2 part-2">
                <div class="round-2 part-2"></div>
              </div>
              <div class="overflow-3">
                <div class="round-3 part-1"></div>
                <div class="color-block"></div>
                <div class="number">02</div>
              </div>
              <div class="overflow-3 part-2">
                <div class="round-3 part-2"></div>
              </div>
              <div class="overflow-4 part-1">
                <div class="round-4 part-1"></div>
                <div class="color-block"></div>
                <div class="number">01</div>
              </div>
              <div class="overflow-4 part-2">
                <div class="round-4 part-2"></div>
              </div>
              <div class="round-center"></div>
            </div>
          </div>
          <div class="chart-titles">
            <p>Lorem Ipsum  is simply</p>
            <p>Contrary to popular belief are</p>
            <p>There are many variations</p>
            <p>passages of Lorem</p>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>

<?php
  //SCOPE
 ?>
<?php include 'partials/top.top.php';?>
<?php include 'head/purecss.php';?>
<?php include 'partials/top.bottom.php';?>

<?php include('partials/nav.php'); ?>

<div id="services" class="services">
	<div class="container">
		<div class="head-one text-center">
			<h2>Servicios</h2>
			<span> </span>
			<p>Actualmente ofrecemos los siguientes servicios.</p>
		</div>
    <?php include('partials/services.content.php'); ?>
</div>


<?php include('partials/team.php'); ?>
<?php include('partials/contact.php'); ?>
<?php include('partials/footer.php'); ?>

 <?php include 'partials/bottom.php';?>
